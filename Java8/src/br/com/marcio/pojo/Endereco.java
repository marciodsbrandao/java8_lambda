package br.com.marcio.pojo;

import java.io.Serializable;

public class Endereco implements Serializable {

	private static final long serialVersionUID = -8438377882978962016L;
	
	private String rua;
	private int numero;
	private String Bairro;
	private String Cidade;
	
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return Bairro;
	}
	public void setBairro(String bairro) {
		Bairro = bairro;
	}
	public String getCidade() {
		return Cidade;
	}
	public void setCidade(String cidade) {
		Cidade = cidade;
	}
}

package br.com.marcio.principal;

import br.com.marcio.pojo.Pessoa;

public interface IValidaPessoa {
	
	public void valida(Pessoa pessoa);

}

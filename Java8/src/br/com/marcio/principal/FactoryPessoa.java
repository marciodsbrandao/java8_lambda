package br.com.marcio.principal;

import java.util.ArrayList;
import java.util.List;

import br.com.marcio.enums.SexoEnum;
import br.com.marcio.pojo.Pessoa;

public class FactoryPessoa {

	public static List<Pessoa> getListPessoas(){
		List<Pessoa> pessoas = new ArrayList<>();
		
		pessoas.add(new Pessoa("Marcio brandao", 38, SexoEnum.MASCULINO));
		pessoas.add(new Pessoa("Patricia Alves", 39, SexoEnum.FEMININO));
		pessoas.add(new Pessoa("Gabriel Henrique", 7, SexoEnum.MASCULINO));
		pessoas.add(new Pessoa("Maria Clara Batista", 5, SexoEnum.FEMININO));
		
		return pessoas;
	}
}
